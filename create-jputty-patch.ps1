
$putty_version="0.74"
cd ..\putty-$putty_version

$original_putty_state = (git log --oneline --reverse | select-object -first 1).split(" ")[0]
write-output "--> checking out to original putty commit $original_putty_state"
git checkout $original_putty_state

write-output "--> squash merging original putty state with master HEAD"
git merge --squash master

write-output "--> committing jputty as single commit"
git commit -m "jputty as single commit"

write-output "--> creating jputty patch"
git format-patch HEAD^ --stdout | Out-File -encoding ASCII ../jputty_patch/jputty-$putty_version.patch

write-output "--> checking out back to master HEAD"
git checkout master

cd ..\jputty_patch


