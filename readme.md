**UX tweaks to PuTTY excellent linux SSH tool**


## Content

minor improvements included in the patch 0.74.

1. increase/decrease ssh session font size in run-time using CTRL + mouse wheel (standard windows zoom behaviour)
2. particular host can have ssh session window position and size and icon pre-defined (Settings->Window->jPuTTY tab)
3. particular host can have ssh session window position and size and icone changed in run-time  (Settings->Window->jPuTTY tab)
4. particular host can have multiple sessions windows UI parameters changes stored, e.g. you open several sessions to the same host and change font, color, window size etc. and next time you open 2nd (3rd...) session it will restore previously stored 2nd (3rd ...) session settings.
5. upon openning putty set focus to 'Saved Sessions' editbox and will filter down the list of session below to those containing text you type, useful when you have dozens of stored sessions and don't want to scroll down trying to find the one you need
---

*Don't have much time to make the code perfect, so presented as is, feel free to critisize or suggest improvements*
